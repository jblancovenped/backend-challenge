# Venped Coding Challenge

Our finance department has a request. They currently have a stack of monthly expense reports waiting to be paid..

This reports consists of three different kind of expenses.

- **TRANSPORTATION** (km):
	- Every kilometer is paid at 0.12 $/km
	- When an employee exceeds 100km per month, the next kms are paid at 0.08 $/km
- **MEAL** (units):
	- We like our employees to enjoy their lunch time, so we decided to give refunds of $10 for the first 3 meals and a $6 refund for the remaining meals of the month.
- **PARKING** ($):
	- Parking expenses are reported in $ and are paid totally until $20 each month. If a report exceeds $20 of parking tickets, the financial department refunds 50% of the exceeding amount.

Examples:

```
Concept           | Units
------------------------------
TRANSPORTATION    | 75
MEAL              | 1
TRANSPORTATION    | 15
MEAL              | 1
MEAL              | 1
PARKING           | 15
TRANSPORTATION    | 20
MEAL              | 1
PARKING           | 20
------------------------------
Total             | $76,30


Concept           | Units
------------------------------
TRANSPORTATION    | 75
MEAL              | 1
TRANSPORTATION    | 45
MEAL              | 1
TRANSPORTATION    | 35
MEAL              | 1
MEAL              | 1
PARKING           | 35
TRANSPORTATION    | 20
MEAL              | 1
PARKING           | 10
------------------------------
Total             | $92,50
```

## To Do
- Create a piece of software, using the technology you rather, capable of resolving the amount to be refunded for each expense report.
- Be written as production-ready code. We expect your code to be covered by some tests.
- Make it work, keep it simple.
